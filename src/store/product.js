import axios from 'axios'

const productModule = {
    state: {
        productlist: []
    },
    mutations: {
        getPorductListProductModule(state) {
            axios.get('http://192.168.75.24:8000/api/products/').then(response => {
                state.productlist = response.data
            });
        },
        updateProductProductModule(state, product){
            var api = "http://192.168.75.24:8000/api/products/" + product.id
            axios.put(api,product).then(response => {
                this.commit('getPorductListProductModule')
                alert(response.data)
            });
        },
        createProductProductModule(state,product){
            var api = "http://192.168.75.24:8000/api/products/"
            axios.post(api,product).then(response => {
                this.commit('getPorductListProductModule')
                alert(response.data)
            });
        }
    }
}

export default productModule