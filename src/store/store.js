import Vue from 'vue'
import Vuex from 'vuex'

import producModule from './product'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        count: 1,
        users:[
            {
                firstname: 'Kuharon',
                lastname: 'Tawandoloh',
                id: 625223
            },
            {
                firstname: 'Kuharon1',
                lastname: 'Tawandoloh',
                id: 625223
            }
        ]
    },
    mutations: {
        addCount(state){
            state.count++
        },
        addUser(state,user){
            state.users.push(user)
        }
    },
    modules:{
        producModule: producModule
    }
})